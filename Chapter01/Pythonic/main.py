# Loop

# numbers = [1,2,3,4,5,6,7,8]
# print('Beginner')
# # Beginner
# for i in range(len(numbers)):
#     print(numbers[i], end='\t')
# print('\nIntermediate')
# # Intermediate
# index = 0
# while index < len(numbers):
#     print(numbers[index], end='\t')
#     index += 1
# print('\nExpert')
# # Expert
# for number in numbers:
#     print(number, end='\t')


# Max in a list
# numbers = [1,2,3,4,5]

# print('Beginner')
# # Beginner
# max_number = 0
# for number in numbers:
#     if number > max_number:
#         max_number = number
# print(max_number)

# print('Intermediate')
# # Intermediate
# max_number= numbers[0]
# for i in range(1, len(numbers)):
#     if numbers[i] > max_number:
#         max_number = numbers[i]
# print(max_number)

# print('Expert')
# # Expert
# max_number=max(numbers)
# print(max_number)


# Check if number is even or odd
# print('Beginner')
# # Beginner
# num = 7
# if num % 2 == 0:
#     print("Even")
# else:
#     print("Odd")

# print('Intermediate')
# # Intermediate
# result = "Even" if num % 2 == 0 else "Odd"
# print(result)

# print('Expert')
# # Expert
# print("Even" if num % 2 == 0 else "Odd")

# Squared numbers
numbers = [1, 2, 3, 4, 5, 6, 7, 8]
print('Beginner')
# Beginner
squared_numbers = []
for num in numbers:
    squared_numbers.append(num ** 2)
print(squared_numbers)

print('Intermediate')
# Intermediate
squared_numbers2 = list(map(lambda x: x**2, numbers))
print(squared_numbers2)

print('Expert')
# Expert
squared_numbers3 = [num ** 2 for num in numbers]
print(squared_numbers3)
